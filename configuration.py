# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.transaction import Transaction


class Configuration(metaclass=PoolMeta):
    __name__ = 'production.configuration'
    company = fields.Many2One('company.company', 'Company')
    warehouse_origin = fields.Many2One('stock.location', 'Warehouse Origin')
    warehouse_target = fields.Many2One('stock.location', 'Warehouse Target')
    production_accounting = fields.Boolean('Production Accounting',
        help='Check this option for create account move')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', int(Transaction().context.get('company')))
        ])
        return res[0]
