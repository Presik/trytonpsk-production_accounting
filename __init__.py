# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import production
from . import account
from . import stock
from . import ir
from . import bom
from . import configuration
from . import sale


def register():
    Pool.register(
        configuration.Configuration,
        production.Production,
        production.ProductionDetailedStart,
        production.ProcessProductionAsyncStart,
        production.ProductionCost,
        production.SubProduction,
        production.SubProductionIn,
        production.RoutingStep,
        account.Move,
        stock.Move,
        ir.Cron,
        bom.BOM,
        bom.BOMDirectCost,
        sale.SaleLine,
        module='production_accounting', type_='model')
    Pool.register(
        production.ProductionForceDraft,
        production.ProductionDetailed,
        production.ProcessProductionAsync,
        production.DoneProductions,
        module='production_accounting', type_='wizard')
    Pool.register(
        production.ProductionDetailedReport,
        module='production_accounting', type_='report')
